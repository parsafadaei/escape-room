using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.Interaction.Toolkit;

public class SceneTransitionManager : MonoBehaviour
{
    public FaceScreen fadeScreen;
    public XRRayInteractor leftinteractor;
    public XRRayInteractor rightinteractor;
    private void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            leftinteractor.useForceGrab = true;
            rightinteractor.useForceGrab = true;
        }
        else
        {
            leftinteractor.useForceGrab = false;
            rightinteractor.useForceGrab = false;
        }

    }
    public void GotoScene(int sceneIndex)
    {
        StartCoroutine(GoToSceneRoutine(sceneIndex));
    }
    IEnumerator GoToSceneRoutine(int sceneIndex)
    {
        fadeScreen.FadeOut();
        if (sceneIndex == 2)
        {
            leftinteractor.useForceGrab = true;
            rightinteractor.useForceGrab = true;
        }
        else
        {
            leftinteractor.useForceGrab = false;
            rightinteractor.useForceGrab = false;
        }
        
        yield return new WaitForSeconds(fadeScreen.fadeDuration);

        SceneManager.LoadScene(sceneIndex);

    }
}

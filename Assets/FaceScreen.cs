using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceScreen : MonoBehaviour
{
    public bool fadeOnStart = true;
    public float fadeDuration = 2;
    public Color fadeColor;
    private Renderer rend;
    void Start()
    {
        rend = GetComponent<Renderer>();
        if(fadeOnStart )
        {
            FadeIn();
        }
    }
    public void FadeIn()
    {
        Fade(1, 0);
    }
    public void FadeOut()
    {
        Fade(0, 1);
    }

    public void Fade(float alphaIN, float alphaOUT)
    {
        StartCoroutine(FadeRoutine(alphaIN, alphaOUT));
    }
    public IEnumerator FadeRoutine(float alphaIN, float alphaOUT)
    {
        float timer = 0;
        while (timer <= fadeDuration) 
        {
            Color newColor = fadeColor;
            newColor.a = Mathf.Lerp(alphaIN, alphaOUT, timer/ fadeDuration);
            rend.material.SetColor("_BaseColor", newColor);
            timer += Time.deltaTime;
            yield return null;
        }
        Color newColor2 = fadeColor;
        newColor2.a = Mathf.Lerp(alphaIN, alphaOUT, timer / fadeDuration);
        rend.material.SetColor("_BaseColor", newColor2);
    }
}

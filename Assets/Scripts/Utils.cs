using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class Utils :MonoBehaviour
{
    public XRSocketInteractor merc;
    public XRSocketInteractor venu;
    public XRSocketInteractor eart;
    public XRSocketInteractor mars;
    public XRSocketInteractor jupi;
    public XRSocketInteractor satr;
    public XRSocketInteractor uran;
    public XRSocketInteractor nept;
    public SceneTransitionManager scene;
    public GameObject wrong;
    public GameObject right;
    public void CheckPlanets()
    {
        if(merc.GetOldestInteractableSelected() != null && venu.GetOldestInteractableSelected() != null && eart.GetOldestInteractableSelected() != null && mars.GetOldestInteractableSelected() != null &&
            jupi.GetOldestInteractableSelected() != null && satr.GetOldestInteractableSelected() != null && uran.GetOldestInteractableSelected() != null && nept.GetOldestInteractableSelected() != null )
        {
            if (merc.GetOldestInteractableSelected().transform.name == "Mercury" && venu.GetOldestInteractableSelected().transform.name == "Venus" && eart.GetOldestInteractableSelected().transform.name == "Earth" &&
                mars.GetOldestInteractableSelected().transform.name == "Mars" && jupi.GetOldestInteractableSelected().transform.name == "Jupiter" && satr.GetOldestInteractableSelected().transform.name == "Saturn" &&
                uran.GetOldestInteractableSelected().transform.name == "Uranus" && nept.GetOldestInteractableSelected().transform.name == "Neptune")
            {
                wrong.SetActive(false);
                right.SetActive(true);
                scene.GotoScene(2);
            }
            else
            {
                wrong.SetActive(true);
            }
        }

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class ActivateRay : MonoBehaviour
{
    public GameObject leftRay;
    public GameObject leftTeleRay;
    public GameObject rightRay;
    public GameObject rightTeleRay;

    public XRDirectInteractor leftActivate;
    public XRDirectInteractor rightActivate;

    public InputActionProperty leftTActivate;
    public InputActionProperty rightTActivate;
    void Update()
    {
        leftRay.SetActive(leftActivate.interactablesSelected.Count == 0);
        rightRay.SetActive(rightActivate.interactablesSelected.Count == 0);
        leftTeleRay.SetActive(leftActivate.interactablesHovered.Count == 0);
        rightTeleRay.SetActive(rightActivate.interactablesHovered.Count == 0);
        leftTeleRay.SetActive(leftTActivate.action.ReadValue<float>() > 0.1f);
        rightTeleRay.SetActive(rightTActivate.action.ReadValue<float>() > 0.1f);
    }
}
